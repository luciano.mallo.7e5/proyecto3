package com.example.proyecto3gatitosmiaumiau

import com.example.proyecto3gatitosmiaumiau.ui.screens.CatsViewModel
import org.junit.Rule
import org.junit.Test
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.example.proyecto3gatitosmiaumiau.ui.screens.CatsUiState
import junit.framework.TestCase.assertTrue

class CatsViewModelTest {
    private val viewModel = CatsViewModel()

    @get:Rule
    val composeTestRule = createAndroidComposeRule<CatlistActivity>()


    @Test
    fun catsPhotosViewModelTest() {
        viewModel.getCatsPhotos()
        // Start the app
        composeTestRule.waitUntil(2000) {
            viewModel.catsUiState != CatsUiState.Loading

        }
        assertTrue(viewModel.catsUiState is CatsUiState.Success)

    }


}