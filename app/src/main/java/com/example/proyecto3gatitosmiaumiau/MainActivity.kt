package com.example.proyecto3gatitosmiaumiau

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.proyecto3gatitosmiaumiau.ui.theme.Proyecto3GatitosMiauMiauTheme



class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Proyecto3GatitosMiauMiauTheme {

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = colors.background
                )
                {
                    ShowLogIn()
                }


            }
        }
    }
}

@Composable
fun ShowLogIn() {

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = colors.primary),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        Image(
            modifier = Modifier.padding(top = 20.dp),
            painter = painterResource(id = R.drawable.cat_login_boarding),
            contentDescription = stringResource(id = R.string.app_name),
            contentScale = ContentScale.Fit,
        )
        Text(
            modifier = Modifier.padding(top = 10.dp),
            text = "User",
        )

        val focusManager = LocalFocusManager.current

        EmailInput(focusManager)
        Text(text = "Password")
        PasswordInput(focusManager)
        LogInButton()


    }

}

@Composable
fun EmailInput(focusManager: FocusManager) {

    var emailText by remember { mutableStateOf(TextFieldValue("")) }
    OutlinedTextField(
        value = emailText,
        onValueChange = { emailText = it },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = colors.primaryVariant,
            textColor = Black
        ),
        label = { Text("User@Robert.com", color = colors.secondary) },
        singleLine = true,
        modifier = Modifier.fillMaxWidth(),
        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
        keyboardOptions = KeyboardOptions.Default.copy(
            imeAction = ImeAction.Next,
            keyboardType = KeyboardType.Email
        ), isError= android.util.Patterns.EMAIL_ADDRESS.matcher(emailText.text).matches()

        )
}

@Composable
fun PasswordInput(focusManager: FocusManager) {
    var passwordText by remember { mutableStateOf(TextFieldValue("")) }
    var passwordVisible by remember { mutableStateOf(false) }

    OutlinedTextField(

        value = passwordText,
        onValueChange = { passwordText = it },
        label = { Text("Password",color = colors.secondary) },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = colors.primaryVariant,
            textColor = Black
        ),
        singleLine = true,
        modifier = Modifier.fillMaxWidth(),
        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
        keyboardOptions = KeyboardOptions.Default.copy(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Password
        ),
        visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
        trailingIcon = {
            var image =
                if (passwordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
            val description = if (passwordVisible) "Hide password" else "Show Password"
            IconButton(onClick = { passwordVisible = !passwordVisible }) {
                Icon(imageVector = image, description)
            }
        }
    )
}

@Composable
fun LogInButton() {
    val context = LocalContext.current

    Button(
        colors = ButtonDefaults.buttonColors(
            backgroundColor = colors.secondary,
            contentColor = colors.background
        ),
        onClick = {
            val intent = Intent(context, CatlistActivity::class.java)
            context.startActivity(intent)
        }, modifier = Modifier.padding(top = 30.dp)
    ) {
        Text(text = "Log in ")
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Proyecto3GatitosMiauMiauTheme {
        ShowLogIn()
    }
}