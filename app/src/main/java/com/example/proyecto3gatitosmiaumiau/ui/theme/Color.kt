package com.example.proyecto3gatitosmiaumiau.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Pink = Color(0xFFFF6AE7)
val DarkGreen = Color(0xFF71C45E)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Blue = Color(0xFF1821C2)
val Violet = Color(0xFF3F2261)
val Black = Color(0xFF000000)