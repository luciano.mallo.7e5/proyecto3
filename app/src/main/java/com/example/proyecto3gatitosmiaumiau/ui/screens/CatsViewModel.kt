package com.example.proyecto3gatitosmiaumiau.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.proyecto3gatitosmiaumiau.model.CatInfo
import com.example.proyecto3gatitosmiaumiau.network.CatsApi
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

sealed interface CatsUiState {
    data class Success(val photos: List<CatInfo>, val SelectCountry: String) : CatsUiState
    object Error : CatsUiState
    object Loading : CatsUiState
}


class CatsViewModel : ViewModel() {
    var catsUiState: CatsUiState by mutableStateOf(CatsUiState.Loading)
        private set

    init {
        getCatsPhotos()
    }


    fun getCatsPhotos() {
        viewModelScope.launch {
            catsUiState = try {
                val listResult = CatsApi.retrofitService.getCatsInfo()
                CatsUiState.Success(listResult, "SelectCountry")

            } catch (e: IOException) {
                CatsUiState.Error
            } catch (e: HttpException) {
                CatsUiState.Error
            }
        }
    }

    fun SetSelectCountry(newSelectCountry: String) {
        //aca se cambia el estado magicamente
        if (catsUiState is CatsUiState.Success) {
            catsUiState =
                (catsUiState as CatsUiState.Success).copy(SelectCountry = newSelectCountry)
        }
    }
}