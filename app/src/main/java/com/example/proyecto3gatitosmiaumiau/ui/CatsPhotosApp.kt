package com.example.proyecto3gatitosmiaumiau.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.proyecto3gatitosmiaumiau.R
import com.example.proyecto3gatitosmiaumiau.ui.screens.CatsViewModel
import com.example.proyecto3gatitosmiaumiau.ui.screens.HomeScreen


@Composable
fun CatsPhotosApp(modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier.fillMaxSize(),
        topBar = {
            TopAppBar(title = { Text(stringResource(R.string.app_name)) })

        }
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colors.background
        ) {
            val catsViewModel: CatsViewModel = viewModel()
            HomeScreen(
                catsUiState = catsViewModel.catsUiState,
                setSelectCountry = { catsViewModel.SetSelectCountry(it) })
        }
    }
}

