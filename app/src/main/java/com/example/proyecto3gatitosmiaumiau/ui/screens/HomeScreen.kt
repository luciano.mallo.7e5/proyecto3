package com.example.proyecto3gatitosmiaumiau.ui.screens

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.proyecto3gatitosmiaumiau.DetailActivity
import com.example.proyecto3gatitosmiaumiau.R
import com.example.proyecto3gatitosmiaumiau.model.CatInfo
import com.example.proyecto3gatitosmiaumiau.ui.theme.Proyecto3GatitosMiauMiauTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Composable
fun HomeScreen(
    catsUiState: CatsUiState,
    modifier: Modifier = Modifier,
    setSelectCountry: (String) -> Unit,
) {
    when (catsUiState) {
        is CatsUiState.Loading -> LoadingScreen(modifier)
        is CatsUiState.Success -> ResultScreen(
            catsUiState.photos,
            catsUiState.SelectCountry,
            setSelectCountry
        )
        is CatsUiState.Error -> ErrorScreen(modifier)
    }
}

/**
 * The home screen displaying the loading message.
 */
@Composable
fun LoadingScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .fillMaxSize()
            .background(color = Color.Gray)
    ) {
        GlideImage(
            imageModel = { R.drawable.loading_cat }, modifier = Modifier
                .size(200.dp),
            imageOptions = ImageOptions(contentDescription = stringResource(R.string.loading))
        )

    }
}

/**
 * The home screen displaying error message
 */
@Composable
fun ErrorScreen(modifier: Modifier = Modifier) {
    Column(
       // contentAlignment = Alignment.Center,
        modifier = modifier
            .background(color = Color.Gray),
        horizontalAlignment= Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(stringResource(R.string.loading_failed))
        GlideImage(
            imageModel = { R.drawable.error_kitty_image }, modifier = Modifier
                .fillMaxWidth() .height(220.dp)
               ,
            imageOptions = ImageOptions(contentScale=ContentScale.Crop,contentDescription = stringResource(R.string.loading_failed))
        )
    }
}

/**
 * The home screen displaying number of retrieved photos.
 */
@Composable
fun ResultScreen(
    catInfoList: List<CatInfo>,
    selectCountry: String,
    setSelectCountry: (String) -> Unit,

) {
    Column {
        var catInfoListFiltered = catInfoList
        DropDownComponent(selectCountry, setSelectCountry)
        if (selectCountry != "SelectCountry") {
            catInfoListFiltered = catInfoList.filter { it.countryCode == selectCountry }
        }
        Spacer(modifier = Modifier.height(15.dp))
        CatsList(catInfoListFiltered)
    }

}


@Composable
fun DropDownComponent(selectCountry: String, setSelectCountry: (String) -> Unit) {
    val listOfCountry =
        arrayOf(
            "SelectCountry",
            "EG",
            "US",
            "CA",
            "ES",
            "BR",
            "GR",
            "AE",
            "AU",
            "FR",
            "GB",
            "MM",
            "CY",
            "RU",
            "CN",
            "JP",
            "TH",
            "IM",
            "NO",
            "IR",
            "SP",
            "SO",
            "TR"
        )
    val contextForToast = LocalContext.current.applicationContext

    // state of the menu
    var expanded by remember {
        mutableStateOf(false)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.Start
    ) {
        Button(onClick = {
            expanded = true
        }, shape = MaterialTheme.shapes.large) {
            Text(text = selectCountry)
            Icon(
                imageVector = Icons.Default.ArrowDropDown,
                contentDescription = "OpenOptions",
            )
        }
        // drop down menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            },
            modifier = Modifier
                .height(250.dp)
                .background(color = MaterialTheme.colors.primary)
        ) {
            // adding items
            listOfCountry.forEach { countryCode ->
                DropdownMenuItem(
                    onClick = {
                        setSelectCountry(countryCode)
                        Toast.makeText(contextForToast, "Country: $countryCode", Toast.LENGTH_SHORT)
                            .show()
                        expanded = false
                    },
                    enabled = (countryCode != selectCountry)
                ) {
                    Text(text = countryCode, color = MaterialTheme.colors.primaryVariant)
                }
            }
        }
    }
}


@Composable
fun CatsList(catsList: List<CatInfo>, modifier: Modifier = Modifier) {
    val mContext = LocalContext.current
    LazyColumn(modifier = modifier) {
        items(items = catsList) { catInfo ->
            Button(
                onClick = {
                    val intent = Intent(mContext, DetailActivity::class.java)
                    val catInfoStr = Json.encodeToString(catInfo)
                    intent.putExtra("CatInfo", catInfoStr)
                    mContext.startActivity(intent)
                }
            ) {
                CatCard(
                    catPhoto = catInfo
                )
            }
        }
    }
}

@Composable
fun CatCard(catPhoto: CatInfo, modifier: Modifier = Modifier) {

    Card(
        modifier = modifier,
        border = BorderStroke(
            1.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.background
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {
            GlideImage(
                imageModel = { catPhoto.image!!.url }, modifier = Modifier
                    .size(200.dp),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )

            Text(
                fontSize = 20.sp,
                text = "Breed:\n" + catPhoto.name,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier.padding(start = 35.dp),
                textAlign = TextAlign.Center,
            )
        }
    }
}


//Previews

@Preview(showBackground = true)
@Composable
fun LoadingScreenPreview() {
    Proyecto3GatitosMiauMiauTheme {
        LoadingScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ErrorScreenPreview() {
    Proyecto3GatitosMiauMiauTheme {
        ErrorScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ResultScreenPreview() {
    Proyecto3GatitosMiauMiauTheme {
        // ResultScreen(photos = PreviewData().loadCats())
    }
}


