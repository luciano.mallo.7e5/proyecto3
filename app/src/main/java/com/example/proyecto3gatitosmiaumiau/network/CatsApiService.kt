package com.example.proyecto3gatitosmiaumiau.network

import com.example.proyecto3gatitosmiaumiau.model.CatInfo
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

private const val BASE_URL =
    "https://api.thecatapi.com/v1/"

private const val token = "live_0j6JrgZRiGZnuIA9YKPMxKFOTYhWGp1kwiUtFWgbSdyj3jM6nA5jQnp4GW7OsTRq"

private val jsonIgnored = Json { ignoreUnknownKeys = true }

/**
 * Use the Retrofit builder to build a retrofit object using a kotlinx.serialization converter
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(jsonIgnored.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

/**
 * Retrofit service object for creating api calls
 */
interface CatsApiService {
    @Headers("x-api-key: $token")
    @GET("breeds")
    suspend fun getCatsInfo(): List<CatInfo>

}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object CatsApi {
    val retrofitService: CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}
